﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = File.ReadAllText(@"c:\Users\Хакер\source\repos\task_4.txt", Encoding.UTF8).Split(';');
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(i % 2 == 0 ? arr[i].ToLower() : arr[i].ToUpper());
            }

        }
    }
}
